import React, { Component } from 'react';
import './Curriculum.css';
import martin__avatar from './img/martinm__avatar_300px.png';

class Curriculum extends Component {
	render() {
		return (
			<div className="curriculum container shadow p-lg-5 p-4 mt-sm-5 mb-sm-5 bg-white">
				<header>
					<section className='row justify-content-center mb-4'>
						<div className='curriculum__avatar text-xs-center text-md-left 								col-5  	col-md-3 	col-lg-3'>
							<img className='curriculum__avatar-img img-fluid'
								src={martin__avatar}
								alt='Foto de Martín Mármol'
							/>
						</div>

						<section className='curriculum__person text-xs-center text-md-left align-self-center 	col-12 	 	col-md-9 	col-lg-9'>
							<h1 className='curriculum__name display-3 color-primary'>Martin Marmol</h1>
							<p className='curriculum__job-position h5'>Desarrollador Front End</p>
						</section>
					</section>

					<section className='row mb-3'>
						<section className='col-12 col-md'>
							<h4 className='color-primary'>Perfil</h4>
							<p className='text-justify'>
								Created meaningful user experiences for products across various industries.
								Using research, positioning, messaging and user experience design, I enjoy
								building brands and products. My deepest expertise is in front end web
								development.</p>
						</section>
						<address className='col-12 col-md-auto'>
							<h4 className='color-primary'>Contacto</h4>
							<ul className='curriculum__details list-unstyled'>
								<li className='mb-1'>
									<svg className='mr-2 curriculum__details-icon' xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M17.5 2c.276 0 .5.224.5.5v19c0 .276-.224.5-.5.5h-11c-.276 0-.5-.224-.5-.5v-19c0-.276.224-.5.5-.5h11zm2.5 0c0-1.104-.896-2-2-2h-12c-1.104 0-2 .896-2 2v20c0 1.104.896 2 2 2h12c1.104 0 2-.896 2-2v-20zm-9.5 1h3c.276 0 .5.224.5.501 0 .275-.224.499-.5.499h-3c-.275 0-.5-.224-.5-.499 0-.277.225-.501.5-.501zm1.5 18c-.553 0-1-.448-1-1s.447-1 1-1c.552 0 .999.448.999 1s-.447 1-.999 1zm5-3h-10v-13h10v13z"/></svg>
									11-6568-4477
								</li>
								<li className='mb-1'>
									<svg className='mr-2 curriculum__details-icon' xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm6.623 7.929l-4.623 5.712v-9.458l4.623 3.746zm-4.141-5.929h19.035l-9.517 7.713-9.518-7.713zm5.694 7.188l3.824 3.099 3.83-3.104 5.612 6.817h-18.779l5.513-6.812zm9.208-1.264l4.616-3.741v9.348l-4.616-5.607z"/></svg>
									martinmarmol.ar@gmail.com
								</li>
								<li className='mb-1'>
									<svg className='mr-2 curriculum__details-icon' xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M12 0c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
									Ciudad de Buenos Aires, Argentina
								</li>
								<li className=''>
									<svg className='mr-2 curriculum__details-icon' xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M0 0v24h24v-24h-24zm8 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.397-2.586 7-2.777 7 2.476v6.759z"/></svg>
									<a href='https://ar.linkedin.com/in/martinarielm' rel="noopener noreferrer" target='_blank'>linkedin.com/in/martinarielm</a>
								</li>
							</ul>
						</address>
					</section>
				</header>

				<main>
					<section className='mb-4'>
						<header className='row mb-3'>
							<h4 className='col-auto mb-2 color-primary'>Experiencia</h4>
							<div className='col'><hr/></div>
						</header>
						<article className='row'>
							<header className='col-md-3 text-xs-left text-md-right'>
								<div className='row align-items-end'>
									<h5 className='col-auto col-md-12 heading-secondary'>mxHero</h5>
									<p className='col col-md-12 mb-1'>
										<span className='badge badge-secondary'>10/2012</span> - <span className='badge badge-secondary'>Actualidad</span>
									</p>
								</div>
							</header>
							<section className='col-md-9'>
								<h6 className='heading-secondary'>Diseñador y desarrollador front end</h6>
								<p className='text-justify'>Lead conceptualization, design and development of mxHero Mail2Cloud
								administration panel. Extensive use of JavaScript (ES6), Mithril
								(framework MVC), HTML5, CSS3. Designed UI for different mxHero
								applications (web, chrome extension and Outlook plugin).</p>
							</section>
						</article>
						<article className='row'>
							<header className='col-md-3 text-xs-left text-md-right'>
								<div className='row align-items-end'>
									<h5 className='col-auto col-md-12 heading-secondary'>Jupiter Fuegos Artificiales</h5>
									<p className='col col-md-12 mb-1'>
										<span className='badge badge-secondary'>12/2011</span> - <span className='badge badge-secondary'>09/2012</span>
									</p>
								</div>
							</header>
							<section className='col-md-9'>
								<h6 className='heading-secondary'>Diseñador gráfico</h6>
								<p className='text-justify'>Encargado del área de diseño gráfico de la empresa. Banners, 
								packaging de productos, catálogos, ploteos, sitios web. Área de 
								espectáculos. Edición de video. Programación de eventos de fuegos 
								especiales. Plataforma Mac y Windows.</p>
							</section>
						</article>
						<article className='row'>
							<header className='col-md-3 text-xs-left text-md-right'>
								<div className='row align-items-end'>
									<h5 className='col-auto col-md-12 heading-secondary'>ASUS</h5>
									<p className='col col-md-12 mb-1'>
										<span className='badge badge-secondary'>07/2010</span> - <span className='badge badge-secondary'>12/2011</span>
									</p>
								</div>
							</header>
							<section className='col-md-9'>
								<h6 className='heading-secondary'>Diseñador Gráfico</h6>
								<p className='text-justify'>Encargado del área de diseño. Creación de flyers, posters, stands 
								y diferentes opciones impresas para productos expuestos en retailers. 
								Newsletters. Diseños para publicidades de diarios.</p>
							</section>
						</article>
						<article className='row'>
							<header className='col-md-3 text-xs-left text-md-right'>
								<div className='row align-items-end'>
									<h5 className='col-auto col-md-12 heading-secondary'>Inside - Comunicación Interna</h5>
									<p className='col col-md-12 mb-1'>
										<span className='badge badge-secondary'>05/2009</span> - <span className='badge badge-secondary'>01/2010</span>
									</p>
								</div>
							</header>
							<section className='col-md-9'>
								<h6 className='heading-secondary'>Diseñador Audiovisual</h6>
								<p className='text-justify'>Diseñador audiovisual a cargo de todos los proyectos del área. 
								Edición y creación de piezas audiovisuales (videos institucionales y 
								presentaciones animadas) para empresas como AB Mauri Hispanoamérica 
								y HSBC, entre otras. Soporte para el área de diseño gráfico. 
								Mantenimiento de sitios web y creación de newsletters. Adaptación de 
								formatos de videos para diferentes formatos (web streaming y dvd 
								video).</p>
							</section>
						</article>
						<article className='row'>
							<header className='col-md-3 text-xs-left text-md-right'>
								<div className='row align-items-end'>
									<h5 className='col-auto col-md-12 heading-secondary'>MLG International, LLC</h5>
									<p className='col col-md-12 mb-1'>
										<span className='badge badge-secondary'>12/2007</span> - <span className='badge badge-secondary'>01/2009</span>
									</p>
								</div>
							</header>
							<section className='col-md-9'>
								<h6 className='heading-secondary'>Diseñador Gráfico y Audiovisual</h6>
								<p className='text-justify'>Edición de imágenes (banners, publicidades) para sitios web internacionales, minisitios en flash (contenido visual, de audio y actionscript) y edición de video.</p>
							</section>
						</article>
					</section>

					<section className='mb-4'>
						<header className='row mb-3'>
							<h4 className='col-auto mb-2 color-primary'>Conocimientos</h4>
							<div className='col'><hr/></div>
						</header>

						<article className='row'>
							<div className='col-1 col-md-3'></div>
							<div className='col-11 col-md-9'>
								<table className='curriculum__skills'>
									<tbody>
										<tr>
											<td><span className='badge badge-primary bgd-primary'>JavaScript (ES6)</span></td>
											<td><span className='badge badge-primary bgd-primary'>JQuery</span></td>
											<td><span className='badge badge-primary bgd-primary'>React.js</span></td>
											<td><span className='badge badge-primary bgd-primary'>Mithril</span></td>
										</tr>
										<tr>
											<td><span className='badge badge-primary bgd-primary'>AngularJS</span></td>
											<td><span className='badge badge-primary bgd-primary'>Bootstrap</span></td>
											<td><span className='badge badge-primary bgd-primary'>Rest APIs</span></td>
											<td><span className='badge badge-primary bgd-primary'>Version Control (Git)</span></td>
										</tr>
										<tr>
											<td><span className='badge badge-primary bgd-primary'>Photoshop</span></td>
											<td><span className='badge badge-primary bgd-primary'>Illustrator</span></td>
											<td><span className='badge badge-primary bgd-primary'>Premiere</span></td>
											<td><span className='badge badge-primary bgd-primary'>After Effects</span></td>
										</tr>
										<tr>
											<td><span className='badge badge-primary bgd-primary'>Sublime IDE</span></td>
											<td><span className='badge badge-primary bgd-primary'>HTML5</span></td>
											<td><span className='badge badge-primary bgd-primary'>CSS3</span></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</article>
					</section>

					<section className='mb-4'>
						<header className='row mb-3'>
							<h4 className='col-auto mb-2 color-primary'>Educación</h4>
							<div className='col'><hr/></div>
						</header>
						<section  className='row'>
							<div className='col-1 col-md-3'></div>
							<div className='col-11 col-md-9'>
								<article>
									<h4>Laboratorio de Software Libre UTN-FRA</h4>
									<p><span className='badge badge-secondary'>2015</span> Workshop AngularJS </p>
								</article>

								<article>
									<h4>EducaciónIT</h4>
									<p><span className='badge badge-secondary'>2015</span> Javascript orientado a diseñadores </p>
								</article>

								<article>
									<h4>ITMaster</h4>
									<p><span className='badge badge-secondary'>2012</span> HTML5 orientado a Diseñadores, HTML5</p>
								</article>

								<article>
									<h4>Image Campus</h4>
									<p> <span className='badge badge-secondary'>2010</span> Animador 3D, Autodesk 3ds Max</p>
								</article>

								<article>
									<h4>Universidad de Palermo</h4>
									<p><span className='badge badge-secondary'>2005</span> - <span className='badge badge-secondary'>2010</span> Diseñador de Imagen y Sonido, Licenciatura</p>
								</article>
							</div>
						</section>
					</section>

					<section>
						<header className='row mb-3'>
							<h4 className='col-auto mb-2 color-primary'>Referencias</h4>
							<div className='col'><hr/></div>
						</header>

						<section className='row'>
							<div className='col-1 col-md-3'></div>
							<div className='col-11 col-md-9'>
								<article>
									<header>
										<h4>
											<a href='https://www.linkedin.com/in/apanagides/' rel="noopener noreferrer" target='_blank'>Alexis Panagides</a>
										</h4>
										<h6>CEO en mxHero</h6>
									</header>
									<p className='text-justify'>Martin is an inspired designer with a deep understanding and
									appreciation for user interface design that is not only beautiful but
									also functional and intuitive. Martin was instrumental in designing many
									of mxHero's highly rated products. Qualified technology press ranging
									from TechCrunch, VentureBeat to Fast Company have praised products that
									he designed. I highly recommend Martin and am available to further discuss
									this remarkable professional with any prospective employer.</p>
								</article>
								<article>
									<header>
										<h4>
											<a href='https://www.linkedin.com/in/bcsantos/' rel="noopener noreferrer" target='_blank'>Bruno Santos</a>
										</h4>
										<h6>COO en mxHero</h6>
									</header>
									<p className='text-justify'>Martín is a very creative professional with excellent user interface skills and outstanding tact to design graphical elements. His projects in our company has always been very innovative and praised. He has several usability insights which can create very practical user interfaces. Keep up the great work!</p>
								</article>
								<article>
									<header>
										<h4>
											<a href='https://www.linkedin.com/in/juanpabloroyo/' rel="noopener noreferrer" target='_blank'>Juan Pablo Royo</a>
										</h4>
										<h6>Software Developer en mxHero</h6>
									</header>
									<p className='text-justify'>Martin is a very self dedicated and passionate 
									professional. He is always looking for the best design in terms of User 
									Experience without compromising business time to market goals. I have been 
									working for many years in IT industry and i think he covers all the aspects 
									a great UI Designer should have and apart from that he is very humble and 
									open minded. It is really a pleasure work with Martin.</p>
								</article>
								<article>
									<header>
										<h4>
											<a href='https://www.linkedin.com/in/lauraverazzi/' rel="noopener noreferrer" target='_blank'>María Laura Verazzi</a>
										</h4>
										<h6>Lider de area de Diseño en Inside - Comunicación Interna</h6>
									</header>
									<p className='text-justify'>Martín, fue un gran compañero de trabajo. 
									Excelente para trabajar en equipo, súper confiable. Él siempre intenta 
									sumar valor a los proyectos. Gran persona.</p>
								</article>
								<article>
									<header>
										<h4>
											<a href='https://www.linkedin.com/in/martin-galarce-516353a/' rel="noopener noreferrer" target='_blank'>Martin Galarce</a>
										</h4>
										<h6>CEO en MLG International, LLC.</h6>
									</header>
									<p className='text-justify'>Martín trabajó para MLG International como diseñador y demostró ser un gran profesional, tanto por la calidad de su trabajo en el área de diseño, como por su comportamiento y manejo con clientes. Tiene un excelente dominio de las últimas tecnologías y de un gran número de herramientas de diseño.</p>
								</article>
							</div>
						</section>
					</section>
				</main>
				<footer className='footer'>
					<a className='curriculum__printable-link mt-4 mb-3' href='https://cv-martin-marmol.firebaseapp.com/' rel="noopener noreferrer" target='_blank'>cv-martin-marmol.firebaseapp.com</a>
				</footer>
			</div>
		);
	}
}

export default Curriculum;