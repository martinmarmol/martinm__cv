import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import Curriculum from './Curriculum';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<Curriculum />, document.getElementById('root'));
registerServiceWorker();
